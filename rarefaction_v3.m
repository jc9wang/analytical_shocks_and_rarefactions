%% HEADER
% Author:       Jeremy Wang, PhD Candidate, University of Waterloo
% Last Update:  2020-02-29
% Description: 
%   @V1
%       - Computes rarefaction properties for ideal and arbitrary cubic EoS
%       - Valid only for pure gases with no phase change
%   @V2 (skipped to V3 to be consistent with normal shock code versioning)
%   @V3
%       - Tried to add in BWR EoS (although values diverge so don't use it)
%       - Added in formatted graphs

clear; clc; close all;
fprintf("Rarefaction wave calculation initiated...\n");

%% NOMENCLATURE, UNIVERSAL CONSTANTS, AND UNITS
% Physical quantities generally follow the format:
% <quantity>_<location>_<calculationmethod>
% e.g. p_1_IG, delta_h_1_miller

% Variables:
% a = constant in the EoS [Pa (m^3 kmol^-1)^2 K^0.5]
% b = constant in the EoS [m^3 kmol^-1]
% c_p = specific heat capacity at constant pressure [J kmol^-1 K^-1]
% c_v = specific heat capacity at constant volume [J kmol^-1 K^-1]
% h = enthalpy [J kmol^-1]
% M = molar mass [kg kmol^-1]
% p = pressure [Pa]
% rho = density [kg m^-3]
% T = temperature [K]
% u = speed [m s^-1]
% v = molar volume [m^3 kmol^-1]

% Subscripts:
% 'miller' = calculated from Miller et al. (2001) paper on DNS
% 'IG' = calculated using ideal gas law with thermally perfect gas

% Universal constants:
R = 8314.4621; % Universal gas constant [J kmol^-1 K^-1]


%% INPUTS
fprintf("Reading spreadsheets with runs and gas properties data...\n");

% Choose name of input file:
filename_runs = 'rarefaction_runs.xlsx';
num_run = 2; % choose which row of the runs to compute
filename_properties = 'gas_properties.xlsx';

% Choose fsolve params
iterations_fsolve_max = 10000;

% Display flags
show_fsolve = 1; % show fsolve iterations
show_graphs_final = 1; % show final graphs

color_ideal = [0, 0, 1];
color_RK = [0, 0.5, 0];
color_SRK = [0.6350, 0.0780, 0.1840];
color_PR = [0.75, 0, 0.75];
size_font = 11;

xlim_low = NaN;
xlim_high = NaN;
% x_plot_interval = (xlim_high - xlim_low)/4;

%% RUN BATCH ANALYSIS
% Convert inputs to structs
properties = table2struct(readtable(filename_properties));
runs = table2struct(readtable(filename_runs));

fprintf("\n-------------------------------------------------------------\n");
fprintf("-------------------------------------------------------------\n\n");
for index_EoS = 1:3
    fprintf("Now processing Run #%d of %d...\n", num_run, length(runs));

    %% PROCESSING INPUTS
    % Retrieve the gas properties:
    gas_name = runs(num_run).gas_name; % get the current gas name
    num_gas = find(contains({properties.gas_name}, gas_name)); % retrieve index to the gas properties table

    gamma(index_EoS) = properties(num_gas).gamma;      % specific heat ratio
    M(index_EoS) = properties(num_gas).M;              % molar mass
    T_c(index_EoS) = properties(num_gas).T_c;          % critical temperature
    p_c(index_EoS) = properties(num_gas).p_c;          % critical pressure
    omega(index_EoS) = properties(num_gas).omega;      % acentric factor

    % Retrieve fit parameters for thermally perfect c_IG_p:
    A = str2num(properties(num_gas).c_IG_p_fit_params);

%     % Retrieve BWR properties:
%     a_BWR(index_EoS) = properties(num_gas).a_BWR;
%     A_BWR(index_EoS) = properties(num_gas).A_BWR;
%     b_BWR(index_EoS) = properties(num_gas).b_BWR;
%     B_BWR(index_EoS) = properties(num_gas).B_BWR;
%     c_BWR(index_EoS) = properties(num_gas).c_BWR;
%     C_BWR(index_EoS) = properties(num_gas).C_BWR;
%     alpha_BWR(index_EoS) = properties(num_gas).alpha_BWR;
%     gamma_BWR(index_EoS) = properties(num_gas).gamma_BWR;

    % Retrieve time, left-hand, right-hand conditions for the current run:
    t(index_EoS) = runs(num_run).t;
    T_5(index_EoS) = runs(num_run).T_5;
    rho_5(index_EoS) = runs(num_run).rho_5;
    v_5(index_EoS) = M(index_EoS)/rho_5(index_EoS);
    u_5(index_EoS) = runs(num_run).u_5;

    T_3(index_EoS) = runs(num_run).T_3;
    v_3(index_EoS) = ((T_5(index_EoS)/T_3(index_EoS)) * v_5(index_EoS)^(gamma(index_EoS)-1))^(1/(gamma(index_EoS)-1));
    rho_3(index_EoS) = M(index_EoS)/v_3(index_EoS);
    
    % Retrieve x-axis interval size
    num_intervals_per_var(index_EoS) = runs(num_run).num_intervals_per_var;

    %% DEFINITIONS & SYMBOLIC FUNCTIONS
    fprintf("Now executing basic definitions and symbolic functions...\n");

    % Set / reset the symbolic variables
    syms p v T A_1 A_2 A_3 A_4 A_5 A_6 x

    % Gas constants
    R_specific = R / M(index_EoS); % [J kg^-1 K^-1]

    % Setting EoS parameters based on input selection
    switch index_EoS
        case 0 % Ideal Gas Law
            EoS_tag = "Ideal and thermally perfect gas";
            a = 0;
            b = 0;
            Theta = 0;
            delta = 0;
            epsilon = 0;
            p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
        case 1 % Redlich Kwong
            EoS_tag = "RK with departure functions relative to thermally perfect gas";
            a = 0.4278 * R^2 * T_c(index_EoS)^(2.5) / p_c(index_EoS);
            b = 0.0867 * R * T_c(index_EoS) / p_c(index_EoS);
            Theta = a * (1/T)^0.5;
            delta = b;
            epsilon = 0;
            p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
        case 2 % Soave-Redlich-Kwong
            EoS_tag = "SRK with departure functions relative to thermally perfect gas";
            a = 0.42747 * R^2 * T_c(index_EoS)^2 / p_c(index_EoS);
            b = 0.08664 * R * T_c(index_EoS) / p_c(index_EoS);
            Theta = a*(1 + (0.48 + 1.574*omega(index_EoS) - 0.176*omega(index_EoS)^2)*(1-(T/T_c(index_EoS))^0.5))^2;
            delta = b;
            epsilon = 0;
            p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
        case 3 % Peng-Robinson
            EoS_tag = "PR with departure functions relative to thermally perfect gas";
            a = 0.45724 * R^2 * T_c(index_EoS)^2 / p_c(index_EoS);
            b = 0.07780 * R * T_c(index_EoS) / p_c(index_EoS);
            Theta = a*(1 + (0.37464 + 1.54226*omega(index_EoS) - 0.2699*omega(index_EoS)^2)*(1-(T/T_c(index_EoS))^0.5))^2;
            delta = 2*b;
            epsilon = -b^2;
            p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
%         case 4 % Benedict Webb Rubin
%             EoS_tag = "BWR with departure functions relative to thermally perfect gas";
%             p = R*T/v + ...
%                 (B_BWR(index_EoS)*R*T - ...
%                 A_BWR(index_EoS) - ...
%                 C_BWR(index_EoS)/T^2)*(1/v^2) + ...
%                 (b_BWR(index_EoS)*R*T - ...
%                 a_BWR(index_EoS))/v^3 + ...
%                 a_BWR(index_EoS)*alpha_BWR(index_EoS)/v^6 + ...
%                 (c_BWR(index_EoS)/(v^3*T^2))*(1 + gamma_BWR(index_EoS)/v^2)*exp(-gamma_BWR(index_EoS)/v^2);
    end
    % NOTE: since virial EoS diverge near criticality and BWR is close to
    % virial, we do not compute here.
    % ALWAYS CHECK THE GRAPHING INDICES AND LABELS TO MAKE SURE THEY ARE CONSISTENT!

    fprintf("\tEoS selection: %s...\n", EoS_tag);

    % Pressure
    p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
    p_IG = (M(index_EoS)/v)*R_specific*T;

    % Derivatives
    exp_dpdT = diff(p, T);
    exp_d2pdT2 = diff(p, T, 2);
    exp_dpdv = diff(p, v);

    % Integrals
    exp_intd2pdT2 = real(int(exp_d2pdT2, v)); % [J kmol^-1 K^-2]

    % Specific heat capacities and enthalpies
    T_mat = T; % reset the length of T_mat (this default value is overridden)
    for i = 1:length(A) 
        T_mat(i,1) = T^(i-1); 
    end

    c_p_IG_TP = R*(A*T_mat); % verified
    c_v_IG_TP = c_p_IG_TP - R; % verified
    c_v = c_v_IG_TP + T*exp_intd2pdT2; % verified
    c_p = c_v - T*exp_dpdT^2/exp_dpdv; % verified

    % Speed of sound
    c = sqrt(-v^2*c_p*exp_dpdv/(M(index_EoS)*c_v));
    c_IG = sqrt(gamma(index_EoS)*R_specific*T);

    % Rarefaction derivations - real gas
    v_RW = exp( A_1*x/t(index_EoS) - A_2);
    p_RW = exp( -A_3*x/t(index_EoS) - A_4);
    u_RW = A_5 + A_6*x/t(index_EoS);
    
    %% COMPUTATIONS
    fprintf("Now performing computations...\n");

    % FIND PROPERTIES AT THE HEAD
    % ---------------------------------------------------------------------
    fprintf("\tFinding properties at the head...\n");

    T = T_5(index_EoS);
    v = M(index_EoS)/rho_5(index_EoS);

    % Real-gas
    p_5(index_EoS) = double(subs(p));
    c_5(index_EoS) = double(subs(c));
    dxdt_head(index_EoS) = u_5(index_EoS) - c_5(index_EoS);
    x_head(index_EoS) = dxdt_head(index_EoS) * t(index_EoS);

    % Ideal-gas
    p_5_IG(index_EoS) = double(subs(p_IG));
    c_5_IG(index_EoS) = double(subs(c_IG));
    dxdt_head_IG(index_EoS) = u_5(index_EoS) - c_5_IG(index_EoS);
    x_head_IG(index_EoS) = dxdt_head_IG(index_EoS) * t(index_EoS);

    % FIND PROPERTIES AT THE TAIL
    % ---------------------------------------------------------------------
    fprintf("\tFinding properties at the tail...\n");

    T = T_3(index_EoS);
    v = M(index_EoS)/rho_3(index_EoS);

    % Ideal-gas - for the purposes of the current computation, u at the
    % head is computed from the ideal gas value
    p_3_IG(index_EoS) = double(subs(p_IG));
    c_3_IG(index_EoS) = double(subs(c_IG));
    % u_3(index_EoS) = (2*c_5_IG(index_EoS)/(gamma(index_EoS)-1)) * (1 - (p_3_IG(index_EoS)/p_5_IG(index_EoS))^((gamma(index_EoS)-1)/2*gamma(index_EoS)));
    u_3(index_EoS) = (2*c_5_IG(index_EoS)/(gamma(index_EoS)-1)) * (1 - c_3_IG(index_EoS)/c_5_IG(index_EoS));
    dxdt_tail_IG(index_EoS) = u_3(index_EoS) - c_3_IG(index_EoS);
    x_tail_IG(index_EoS) = dxdt_tail_IG(index_EoS) * t(index_EoS);
    
    % Real-gas
    p_3(index_EoS) = double(subs(p));
    c_3(index_EoS) = double(subs(c));
    dxdt_tail(index_EoS) = u_3(index_EoS) - c_3(index_EoS);
    x_tail(index_EoS) = dxdt_tail(index_EoS) * t(index_EoS);


    % CHECK THAT RAREFACTION IS INDEED LEFT-RUNNING!
    % ---------------------------------------------------------------------
    fprintf("\tChecking that rarefaction is LEFT-running...\n");

    if p_5(index_EoS) <= p_3(index_EoS)
        fprintf("Left-hand pressure is less than or equal to right-hand pressure! This code only handles left-moving stationary shocks. Ending...");
        % FLAG change this in the future for the full shock tube code
        return;
    end

    % USE HEAD & TAIL PROPERTIES TO FIND REAL-GAS INTEGRATION CONSTANTS
    % ---------------------------------------------------------------------
    % Note: the A_1 and A_2 solver vs. A_3 and A_4 are separated to help
    % the nonlinear solver converge faster.

    fprintf("\tFinding A_1,A_2,A_3,A_4 integration constants...\n");

    % Find A_1 and A_2 through linear algebra
    A_1 = (t(index_EoS)/(x_head(index_EoS) - x_tail(index_EoS))) * (log(v_5(index_EoS)/v_3(index_EoS)));
    A_2 = -0.5*log(v_5(index_EoS)*v_3(index_EoS)) + 0.5*log(v_5(index_EoS)/v_3(index_EoS))*(x_head(index_EoS) + x_tail(index_EoS))/(x_head(index_EoS) - x_tail(index_EoS));
        
    % Find A_3 and A_4 through linear algebra
    A_3 = -(t(index_EoS)/(x_head(index_EoS) - x_tail(index_EoS))) * (log(p_5(index_EoS)/p_3(index_EoS)));
    A_4 = -0.5*log(p_5(index_EoS)*p_3(index_EoS)) + 0.5*log(p_5(index_EoS)/p_3(index_EoS))*(x_head(index_EoS) + x_tail(index_EoS))/(x_head(index_EoS) - x_tail(index_EoS));
    
    % Find A_5 and A_6 through linear algebra
    A_5 = 0.5*(u_5(index_EoS) + u_3(index_EoS)) - ...
        0.5*(u_5(index_EoS) - c_5(index_EoS) + u_3(index_EoS) - c_3(index_EoS))*(u_5(index_EoS) - u_3(index_EoS))/(u_5(index_EoS)-c_5(index_EoS) - u_3(index_EoS) + c_3(index_EoS));
    A_6 = (u_5(index_EoS) - u_3(index_EoS))/(u_5(index_EoS) - c_5(index_EoS) - u_3(index_EoS) + c_3(index_EoS));
    
    % EVALUATE PROPERTIES ALONG x AT TIME t (IDEAL GAS)
    % -----------------------------------------------------------------  
    fprintf("\tEvaluating properties along x at time t (ideal gas)...\n");

    x_discr_IG = x_head_IG(index_EoS):(x_tail_IG(index_EoS)-x_head_IG(index_EoS))/num_intervals_per_var(index_EoS):x_tail_IG(index_EoS);
    for j = 1:length(x_discr_IG)
        x = x_discr_IG(j);

        % See Modern Compressible Flow (3e) by Anderson for the equations
        u_RW_IG_solved(index_EoS,j) = (2/(gamma(index_EoS) + 1))*(c_5_IG(index_EoS) + x/t(index_EoS)); % FLAG: this equation needs to change if left velocity non zero
        % u_RW_IG_solved(index_EoS,j) = (u_3(index_EoS) - u_5(index_EoS))/(x_tail_IG(index_EoS) - x_head_IG(index_EoS))*((j-1)*x_interval(index_EoS)) + u_5(index_EoS);
        T_RW_IG_solved(index_EoS,j) = T_5(index_EoS)*(1 - ((gamma(index_EoS)-1)/2) * (u_RW_IG_solved(index_EoS,j)/c_5_IG(index_EoS)))^2;
        rho_RW_IG_solved(index_EoS,j) = rho_5(index_EoS)*(T_RW_IG_solved(index_EoS,j)/T_5(index_EoS))^(1/(gamma(index_EoS)-1));
        v_RW_IG_solved(index_EoS,j) = M(index_EoS)/rho_RW_IG_solved(index_EoS,j);
        p_RW_IG_solved(index_EoS,j) = rho_RW_IG_solved(index_EoS,j)*R_specific*T_RW_IG_solved(index_EoS,j);
    end

    % EVALUATE PROPERTIES ALONG x AT TIME t (REAL GAS)
    % -----------------------------------------------------------------
    fprintf("\tEvaluating properties along x at time t (real gas)...\n");

    x_discr(index_EoS,:) = x_head(index_EoS):(x_tail(index_EoS)-x_head(index_EoS))/num_intervals_per_var(index_EoS):x_tail(index_EoS); 
    for j = 1:length(x_discr(index_EoS,:))
        x = x_discr(index_EoS,j);

        % Evaluate p_RW and v_RW using A_1, A_2 found above:
        p_RW_solved(index_EoS,j) = double(subs(p_RW));
        v_RW_solved(index_EoS,j) = double(subs(v_RW));
        rho_RW_solved(index_EoS,j) = M(index_EoS)/v_RW_solved(index_EoS,j);

        % Use the Trust-Region-Doglet solver to get T_RW:
        syms v T;
        v = v_RW_solved(index_EoS,j);

        eqn_p = subs(p - p_RW_solved(index_EoS,j));
        f_eqn_p = matlabFunction(eqn_p);
        f_eqn_pstr = func2str(f_eqn_p);
        f_eqn_pstr = strrep(f_eqn_pstr, '@(T)', '@(x)'); 
        f_eqn_pstr = strrep(f_eqn_pstr, 'T', 'x');
        f_eqn_p_matlabfunction = str2func(f_eqn_pstr);
        if show_fsolve
            S = fsolve(f_eqn_p_matlabfunction, T_RW_IG_solved(index_EoS,j), optimoptions('fsolve', 'Display', 'iter', 'MaxFunctionEvaluations', iterations_fsolve_max));
        else
            S = fsolve(f_eqn_p_matlabfunction, T_RW_IG_solved(index_EoS,j), optimoptions('fsolve', 'MaxFunctionEvaluations', iterations_fsolve_max));
        end
        T_RW_solved(index_EoS,j) = S;

        % Substitute rho = M/v into continuity equation and solve for u:
        u_RW_solved(index_EoS,j) = double(subs(u_RW));
    end
end

%% OUTPUTS - FINAL GRAPHS
fprintf("\nNow displaying final graphs...\n");

if show_graphs_final
    figure; 
    subplot(2,2,1); hold on;
    plot([xlim_low, x_discr_IG, xlim_high], [p_RW_IG_solved(1,1), p_RW_IG_solved(1,:), p_RW_IG_solved(1,end)], '-', 'Color', color_ideal);
    plot([xlim_low, x_discr(1,:), xlim_high], [p_RW_solved(1,1), p_RW_solved(1,:), p_RW_solved(1,end)], '-', 'Color', color_RK);
    plot([xlim_low, x_discr(2,:), xlim_high], [p_RW_solved(2,1), p_RW_solved(2,:), p_RW_solved(2,end)], '-', 'Color', color_SRK);
    plot([xlim_low, x_discr(3,:), xlim_high], [p_RW_solved(3,1), p_RW_solved(3,:), p_RW_solved(3,end)], '-', 'Color', color_PR);
    xlabel('x');
%     xlim([xlim_low xlim_high]);
%     xticks(xlim_low:x_plot_interval:xlim_high);
    ylabel('p');
%     ylim([6e4 12e4]);
    set(gca, 'fontsize', size_font);
    
    subplot(2,2,2); hold on;
    plot([xlim_low, x_discr_IG, xlim_high], [v_RW_IG_solved(1,1), v_RW_IG_solved(1,:), v_RW_IG_solved(1,end)], '-', 'Color', color_ideal);
    plot([xlim_low, x_discr(1,:), xlim_high], [v_RW_solved(1,1), v_RW_solved(1,:), v_RW_solved(1,end)], '-', 'Color', color_RK);
    plot([xlim_low, x_discr(2,:), xlim_high], [v_RW_solved(2,1), v_RW_solved(2,:), v_RW_solved(2,end)], '-', 'Color', color_SRK);
    plot([xlim_low, x_discr(3,:), xlim_high], [v_RW_solved(3,1), v_RW_solved(3,:), v_RW_solved(3,end)], '-', 'Color', color_PR);
    xlabel('x');
%     xlim([xlim_low xlim_high]);
%     xticks(xlim_low:x_plot_interval:xlim_high);
    ylabel('\nu');
%     ylim([22 34]);
    set(gca, 'fontsize', size_font);
    
    subplot(2,2,3); hold on;
    plot([xlim_low, x_discr_IG, xlim_high], [T_RW_IG_solved(1,1), T_RW_IG_solved(1,:), T_RW_IG_solved(1,end)], '-', 'Color', color_ideal);
    plot([xlim_low, x_discr(1,:), xlim_high], [T_RW_solved(1,1), T_RW_solved(1,:), T_RW_solved(1,end)], '-', 'Color', color_RK);
    plot([xlim_low, x_discr(2,:), xlim_high], [T_RW_solved(2,1), T_RW_solved(2,:), T_RW_solved(2,end)], '-', 'Color', color_SRK);
    plot([xlim_low, x_discr(3,:), xlim_high], [T_RW_solved(3,1), T_RW_solved(3,:), T_RW_solved(3,end)], '-', 'Color', color_PR);
    xlabel('x');
%     xlim([xlim_low xlim_high]);
%     xticks(xlim_low:x_plot_interval:xlim_high);
    ylabel('T');
%     ylim([270 310]);
    set(gca, 'fontsize', size_font);
    
    subplot(2,2,4); hold on;
    plot([xlim_low, x_discr_IG, xlim_high], [u_RW_IG_solved(1,1), u_RW_IG_solved(1,:), u_RW_IG_solved(1,end)], '-', 'Color', color_ideal);
    plot([xlim_low, x_discr(1,:), xlim_high], [u_RW_solved(1,1), u_RW_solved(1,:), u_RW_solved(1,end)], '-', 'Color', color_RK);
    plot([xlim_low, x_discr(2,:), xlim_high], [u_RW_solved(2,1), u_RW_solved(2,:), u_RW_solved(2,end)], '-', 'Color', color_SRK);
    plot([xlim_low, x_discr(3,:), xlim_high], [u_RW_solved(3,1), u_RW_solved(3,:), u_RW_solved(3,end)], '-', 'Color', color_PR);
    xlabel('x');
%     xlim([xlim_low xlim_high]);
%     xticks(xlim_low:x_plot_interval:xlim_high);
    ylabel('u');
%     ylim([0 100]);
%     yticks(0:25:100);
    set(gca, 'fontsize', size_font);
    
    % Redundant figure just to extract legend
    figure; hold on;
    plot(x_discr_IG(1,:), u_RW_IG_solved(1,:), '-', 'Color', color_ideal);
    plot(x_discr(1,:), u_RW_solved(1,:), '-', 'Color', color_RK);
    plot(x_discr(2,:), u_RW_solved(2,:), '-', 'Color', color_SRK);
    plot(x_discr(3,:), u_RW_solved(3,:), '-', 'Color', color_PR);
    legend('Ideal','RK','SRK','PR','Location', 'southoutside', 'Orientation', 'horizontal');
    set(gca, 'fontsize', size_font);
end

%% END
fprintf('Program execution completed.\n');