%% HEADER
% Author:       Jeremy Wang, PhD Candidate, University of Waterloo
% Last Update:  2020-02-29
% Description: 
%   @V1
%       - Computes normal shock relations for ideal and arbitrary cubic EoS
%       - Valid only for pure gases with no phase change
%   @V2
%       - Cleaned up nomenclature and units
%       - Added in further comments to explain code
%       - Updated sensitivity analysis graphs
%       - Added in batch computation using spreadsheets
%   @V3
%       - Tried to add in BWR EoS (although values diverge so don't use it)
%       - Added in formatted graphs

clear; clc; close all;
fprintf("Normal shock calculation initiated...\n");

%% NOMENCLATURE, UNIVERSAL CONSTANTS, AND UNITS
% Physical quantities generally follow the format:
% <quantity>_<location>_<calculationmethod>
% e.g. p_1_IG, delta_h_1_miller

% Variables:
% a = constant in the EoS [Pa (m^3 kmol^-1)^2 K^0.5]
% b = constant in the EoS [m^3 kmol^-1]
% c = speed of sound [m s^-1]
% c_p = specific heat capacity at constant pressure [J kmol^-1 K^-1]
% c_v = specific heat capacity at constant volume [J kmol^-1 K^-1]
% h = enthalpy [J kmol^-1]
% M = molar mass [kg kmol^-1]
% p = pressure [Pa]
% rho = density [kg m^-3]
% T = temperature [K]
% u = speed [m s^-1]
% v = molar volume [m^3 kmol^-1]

% Subscripts:
% 'miller' = calculated from Miller et al. (2001) paper on DNS
% 'IG' = calculated using ideal gas law with thermally perfect gas

% Universal constants:
R = 8314.4621; % Universal gas constant [J kmol^-1 K^-1]


%% INPUTS
fprintf("Reading spreadsheets with runs and gas properties data...\n");

% Choose name of input file:
filename_runs = 'normal_shock_runs_constp.xlsx';
filename_properties = 'gas_properties.xlsx';

% Choose fsolve params
iterations_fsolve_max = 10000;
prefactor = 1; % don't change unless you got a good reason to guess something besides IG

% Display flags
show_fsolve = 0; % show fsolve iterations?
show_graph_sensitivity_cpcv = 0; % show heat capacity sensitivity graphs
show_graph_sensitivity_c = 0; % show speed of sound sensitivity graphs
show_graph_sensitivity_h_IG = 0; % show ideal gas enthalpy graphs
show_graph_sensitivity_deltah = 0; % show enthalpy departure graphs
show_graph_sensitivity_h = 0; % show real gas enthalpy graphs
show_graph_sensitivity_goveqn = 0; % show CME/CEE graphs
show_graphs_final = 1; % show final shock ratio graphs

color_ideal = [0, 0, 1];
color_RK = [0, 0.5, 0];
color_SRK = [0.6350, 0.0780, 0.1840];
color_PR = [0.75, 0, 0.75];
size_font = 11;
size_font_sensitivity = 11;
xlim_low = 1;
xlim_high = 4;

index_SUBC = 1; % run with subcritical case of interest
index_TRAC = 5; % run with transcritical case of interest
index_SUPC = 10; % run with supercritical case of interest

%% RUN BATCH ANALYSIS
% Convert inputs to structs
properties = table2struct(readtable(filename_properties));
runs = table2struct(readtable(filename_runs));

fprintf("\n-------------------------------------------------------------\n");
fprintf("-------------------------------------------------------------\n\n");
for index_EoS = 1:3
    for num_run = 1:length(runs)
        fprintf("Now processing Run #%d of %d...\n", num_run, length(runs));

        %% PROCESSING INPUTS
        % Retrieve the gas properties:
        gas_name = runs(num_run).gas_name; % get the current gas name
        num_gas = find(contains({properties.gas_name}, gas_name)); % retrieve index to the gas properties table

        gamma(index_EoS, num_run) = properties(num_gas).gamma;      % specific heat ratio
        M(index_EoS, num_run) = properties(num_gas).M;              % molar mass
        T_c(index_EoS, num_run) = properties(num_gas).T_c;          % critical temperature
        p_c(index_EoS, num_run) = properties(num_gas).p_c;          % critical pressure
        omega(index_EoS, num_run) = properties(num_gas).omega;      % acentric factor
        h_ref(index_EoS, num_run) = properties(num_gas).h_ref;      % reference enthalpy 
        
        % NOTE: h_ref is never explicitly computed, it only appears during
            % integration and cancels out in the equtaions that need to be solved. 
            % Thus, h_ref is included only for completeness but it does NOT affect 
            % shock solution, so a trivial value of zero is used.  

        % Retrieve fit parameters for thermally perfect c_IG_p:
        A = str2num(properties(num_gas).c_IG_p_fit_params);
        
%         % Retrieve BWR properties:
%         a_BWR(index_EoS, num_run) = properties(num_gas).a_BWR;
%         A_BWR(index_EoS, num_run) = properties(num_gas).A_BWR;
%         b_BWR(index_EoS, num_run) = properties(num_gas).b_BWR;
%         B_BWR(index_EoS, num_run) = properties(num_gas).B_BWR;
%         c_BWR(index_EoS, num_run) = properties(num_gas).c_BWR;
%         C_BWR(index_EoS, num_run) = properties(num_gas).C_BWR;
%         alpha_BWR(index_EoS, num_run) = properties(num_gas).alpha_BWR;
%         gamma_BWR(index_EoS, num_run) = properties(num_gas).gamma_BWR;

        % Retrieve pre-shock conditions for the current run:
        T_1(index_EoS, num_run) = runs(num_run).T_1;
        rho_1(index_EoS, num_run) = runs(num_run).rho_1;
        u_1(index_EoS, num_run) = runs(num_run).u_1;

        % Retrieve sensitivity analysis parameters for the current run:
        upper_lim_factor(index_EoS, num_run) = runs(num_run).upper_lim_factor; 
        lower_lim_factor(index_EoS, num_run) = runs(num_run).lower_lim_factor; 
        num_intervals_per_var(index_EoS, num_run) = runs(num_run).num_intervals_per_var;

            % NOTE: code will compute the nonlinear normal shock functions over the domain:
            % {v, T | v in [v_2*lower_lim:v_2*upper_lim], T in [v_2*lower_lim:v_2*upper_lim]}
            % where v, T are each computed at num_intervals_per_var points uniformly distributed over the domain


        %% DEFINITIONS & SYMBOLIC FUNCTIONS
        fprintf("Now executing basic definitions and symbolic functions...\n");

        % Set / reset the symbolic variables
        syms p v T

        % Gas constants
        R_specific = R / M(index_EoS, num_run); % [J kg^-1 K^-1]

        % Setting EoS parameters based on input selection
        switch index_EoS
            case 0 % Ideal Gas Law
                EoS_tag = "Ideal and thermally perfect gas";
                a = 0;
                b = 0;
                Theta = 0;
                delta = 0;
                epsilon = 0;
                p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
            case 1 % Redlich Kwong
                EoS_tag = "RK with departure functions relative to thermally perfect gas";
                a = 0.4278 * R^2 * T_c(index_EoS, num_run)^(2.5) / p_c(index_EoS, num_run);
                b = 0.0867 * R * T_c(index_EoS, num_run) / p_c(index_EoS, num_run);
                Theta = a * (1/T)^0.5;
                delta = b;
                epsilon = 0;
                p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
            case 2 % Soave-Redlich-Kwong
                EoS_tag = "SRK with departure functions relative to thermally perfect gas";
                a = 0.42747 * R^2 * T_c(index_EoS, num_run)^2 / p_c(index_EoS, num_run);
                b = 0.08664 * R * T_c(index_EoS, num_run) / p_c(index_EoS, num_run);
                Theta = a*(1 + (0.48 + 1.574*omega(index_EoS, num_run) - 0.176*omega(index_EoS, num_run)^2)*(1-(T/T_c(index_EoS, num_run))^0.5))^2;
                delta = b;
                epsilon = 0;
                p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
            case 3 % Peng-Robinson
                EoS_tag = "PR with departure functions relative to thermally perfect gas";
                a = 0.45724 * R^2 * T_c(index_EoS, num_run)^2 / p_c(index_EoS, num_run);
                b = 0.07780 * R * T_c(index_EoS, num_run) / p_c(index_EoS, num_run);
                Theta = a*(1 + (0.37464 + 1.54226*omega(index_EoS, num_run) - 0.2699*omega(index_EoS, num_run)^2)*(1-(T/T_c(index_EoS, num_run))^0.5))^2;
                delta = 2*b;
                epsilon = -b^2;
                p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
%             case 4 % Benedict Webb Rubin
%                 EoS_tag = "BWR with departure functions relative to thermally perfect gas";
%                 p = R*T/v + ...
%                     (B_BWR(index_EoS, num_run)*R*T - ...
%                     A_BWR(index_EoS, num_run) - ...
%                     C_BWR(index_EoS, num_run)/T^2)*(1/v^2) + ...
%                     (b_BWR(index_EoS, num_run)*R*T - ...
%                     a_BWR(index_EoS, num_run))/v^3 + ...
%                     a_BWR(index_EoS, num_run)*alpha_BWR(index_EoS, num_run)/v^6 + ...
%                     (c_BWR(index_EoS, num_run)/(v^3*T^2))*(1 + gamma_BWR(index_EoS, num_run)/v^2)*exp(-gamma_BWR(index_EoS, num_run)/v^2);
        end
        % NOTE: since virial EoS diverge near criticality and BWR is close to
        % virial, we do not compute here.
        % ALWAYS CHECK THE GRAPHING INDICES AND LABELS TO MAKE SURE THEY ARE CONSISTENT!

        fprintf("\tEoS selection: %s...\n", EoS_tag);

        % Derivatives
        dThetadT = diff(Theta, T);
        d2ThetadT2 = diff(Theta, T, 2);
        exp_dpdT = diff(p, T);
        exp_d2pdT2 = diff(p, T, 2);
        exp_dpdv = diff(p, v);

        % Derivatives manually written out
        % exp_dpdT = R/(v-b)-(dThetadT)/(v^2+delta*v+epsilon);
        % exp_d2pdT2 = -(d2ThetadT2)/(v^2+delta*v+epsilon);
        % exp_dpdv = R*T/(v-b)^2+Theta*(2*v+delta)/(v^2+delta*v+epsilon)^2;

        % Integrals
        exp_intdpdT = real(int(exp_dpdT, v)); % [J kmol^-1 K^-1]
        exp_intvdpdv = real(int(v*exp_dpdv, v)); % [J kmol^-1]
        exp_intd2pdT2 = real(int(exp_d2pdT2, v)); % [J kmol^-1 K^-2]

        % Integrals manually written out
        % exp_intdpdT = R*log(v-b) + dThetadT*log((sqrt(delta^2-4*epsilon)+2*v+delta)/(sqrt(delta^2-4*epsilon)-2*v-delta))/(sqrt(delta^2-4*epsilon));
        % exp_intvdpdv = -Theta*v/(v^2+delta*v+epsilon) - Theta*log((sqrt(delta^2-4*epsilon)+2*v+delta)/(sqrt(delta^2-4*epsilon)-2*v-delta))/(sqrt(delta^2-4*epsilon)) ...
        %     - R*T*log(v-b) + R*T*b/(v-b);
        % exp_intd2pdT2 = d2ThetadT2*log((sqrt(delta^2-4*epsilon)+2*v+delta)/(sqrt(delta^2-4*epsilon)-2*v-delta))/(sqrt(delta^2-4*epsilon));

        % Specific heat capacities and enthalpies
        T_mat = T; % reset the length of T_mat (this default value is overridden)
        for i = 1:length(A) 
            T_mat(i,1) = T^(i-1); 
        end

        c_p_IG_TP = R*(A*T_mat); % verified
        c_v_IG_TP = c_p_IG_TP - R; % verified
        c_v = c_v_IG_TP + T*exp_intd2pdT2; % verified
        c_p = c_v - T*exp_dpdT^2/exp_dpdv; % verified
        delta_h_IG_TP = real((Theta-T*dThetadT)*log((sqrt(delta^2-4*epsilon)+2*v+delta)/(sqrt(delta^2-4*epsilon)-2*v-delta))/(sqrt(delta^2-4*epsilon)) + R*T - p*v);
        % NOTE: delta_h_IG was taken directly from Maple's computation of
        % {-T*exp_intdpdT - exp_intvdpdv}. For some reason, using the following diverges:
        % delta_h_IG_TP = -T*exp_intdpdT - exp_intvdpdv;

        if index_EoS == 0 % if ideal gas EoS selected, delta_h_IG evaluates to a NaN when it should be zero.
            delta_h_IG_TP = 0;
        end

        h_IG_TP = int(c_p_IG_TP, T) - h_ref(index_EoS, num_run);
        h = h_IG_TP - delta_h_IG_TP;

        % Miller & Bellan (2001) results for Peng-Robinson:
        K_1 = (1/(2*sqrt(2)*b))*log( (v+(1-sqrt(2))*b)/(v+(1+sqrt(2))*b) );
        delta_h_miller = - p*v + R*T - K_1*(Theta - T*dThetadT);
        h_miller = h_IG_TP - delta_h_miller;

        % Speed of sound
        c = sqrt(-v^2*c_p*exp_dpdv/(M(index_EoS, num_run)*c_v));


        %% COMPUTATIONS
        fprintf("Now performing computations...\n");

        % Set (v,T) to pre-shock values to compute pre-shock conditions:
        fprintf("\tPre-shock conditions set...\n");
        
        T = T_1(index_EoS, num_run); 
        v_1(index_EoS, num_run) = M(index_EoS, num_run)/rho_1(index_EoS, num_run);
        v = v_1(index_EoS, num_run);

        % COMPUTE PRE-SHOCK CONDITIONS (IDEAL GAS)
        % -------------------------------------------------------------------------
        fprintf("\tComputing pre-shock conditions (ideal gas)...\n");

        % Ideal gas thermodynamics:
        p_1_IG(index_EoS, num_run) = rho_1(index_EoS, num_run)*R_specific*T_1(index_EoS, num_run);
        c_p_1_IG_TP(index_EoS, num_run) = double(subs(c_p_IG_TP));
        c_v_1_IG_TP(index_EoS, num_run) = double(subs(c_v_IG_TP));
        c_1_IG_CP(index_EoS, num_run) = sqrt(gamma(index_EoS, num_run)*R*T_1(index_EoS, num_run)/M(index_EoS, num_run));
        Ma_1_IG_CP(index_EoS, num_run) = u_1(index_EoS, num_run)/c_1_IG_CP(index_EoS, num_run);
        h_1_IG_TP(index_EoS, num_run) = double(subs(h_IG_TP));

        % COMPUTE PRE-SHOCK CONDITIONS (REAL GAS)
        % -------------------------------------------------------------------------
        fprintf("\tComputing pre-shock conditions (real gas)...\n");

        % Real-gas derivatives and integrals:
        exp_dpdT_1(index_EoS, num_run) = double(subs(exp_dpdT));
        exp_dpdv_1(index_EoS, num_run) = double(subs(exp_dpdv));
        exp_d2pdT2_1(index_EoS, num_run) = double(subs(exp_d2pdT2));
        exp_intdpdT_1(index_EoS, num_run) = double(subs(exp_intdpdT));
        exp_intvdpdv_1(index_EoS, num_run) = double(subs(exp_intvdpdv));
        exp_intd2pdT2_1(index_EoS, num_run) = double(subs(exp_intd2pdT2));

        % Real-gas thermodynamics:
        p_1(index_EoS, num_run) = double(subs(p));
        c_p_1(index_EoS, num_run) = double(subs(c_p));
        c_v_1(index_EoS, num_run) = double(subs(c_v));
        c_1(index_EoS, num_run) = double(subs(c));
        Ma_1(index_EoS, num_run) = u_1(index_EoS, num_run)/c_1(index_EoS, num_run);
        delta_h_1_IG_TP(index_EoS, num_run) = double(subs(delta_h_IG_TP));
        delta_h_1_miller(index_EoS, num_run) = double(subs(delta_h_miller));
        h_1(index_EoS, num_run) = double(subs(h));

        % COMPUTING POST-SHOCK CONDITIONS (IDEAL GAS)
        % -------------------------------------------------------------------------
        fprintf("\tComputing post-shock conditions (ideal gas)...\n");

        % See Modern Compressible Flow (3e) by Anderson for the equations
        p2p1_IG_CP(index_EoS, num_run) = 1 + 2*gamma(index_EoS, num_run)*(Ma_1_IG_CP(index_EoS, num_run)^2 - 1)/(gamma(index_EoS, num_run)+1);
        v2v1_IG_CP(index_EoS, num_run) = (2 + (gamma(index_EoS, num_run)-1)*Ma_1_IG_CP(index_EoS, num_run)^2)/((gamma(index_EoS, num_run)+1)*Ma_1_IG_CP(index_EoS, num_run)^2);
        T2T1_IG_CP(index_EoS, num_run) = p2p1_IG_CP(index_EoS, num_run) * v2v1_IG_CP(index_EoS, num_run);
        u2u1_IG_CP(index_EoS, num_run) = v2v1_IG_CP(index_EoS, num_run); % true by definition from the continuity equation

        p_2_IG_CP(index_EoS, num_run) = p2p1_IG_CP(index_EoS, num_run) * p_1_IG(index_EoS, num_run);
        v_2_IG_CP(index_EoS, num_run) = v2v1_IG_CP(index_EoS, num_run) * v_1(index_EoS, num_run);
        T_2_IG_CP(index_EoS, num_run) = T2T1_IG_CP(index_EoS, num_run) * T_1(index_EoS, num_run);
        u_2_IG_CP(index_EoS, num_run) = u2u1_IG_CP(index_EoS, num_run) * u_1(index_EoS, num_run);

        c_2_IG_CP(index_EoS, num_run) = sqrt(gamma(index_EoS, num_run)*R*T_2_IG_CP(index_EoS, num_run)/M(index_EoS, num_run));
        Ma_2_IG_CP(index_EoS, num_run) = u_2_IG_CP(index_EoS, num_run)/c_2_IG_CP(index_EoS, num_run);
        Ma2Ma1_IG_CP(index_EoS, num_run) = Ma_2_IG_CP(index_EoS, num_run) / Ma_1_IG_CP(index_EoS, num_run);

        % COMPUTING POST-SHOCK CONDITIONS (REAL GAS)
        % -------------------------------------------------------------------------
        fprintf("\tComputing post-shock conditions (real gas)...\n");

        % Define handle to a matrix of nonlinear functions based on governing equations:
        syms v T
        eqn_ns_1 = p - p_1(index_EoS, num_run) - (u_1(index_EoS, num_run)^2/(v_1(index_EoS, num_run)/M(index_EoS, num_run))^2)*((v_1(index_EoS, num_run)/M(index_EoS, num_run))-(v/M(index_EoS, num_run))); % [Pa]
        eqn_ns_2 = (h - h_1(index_EoS, num_run))/M(index_EoS, num_run) - (u_1(index_EoS, num_run)^2/2)*(1 - v^2/v_1(index_EoS, num_run)^2); % [J kg^-1]
        mat_ns = [eqn_ns_1; eqn_ns_2];
        f = matlabFunction(mat_ns);
        f_str = func2str(f);
        f_str = strrep(f_str, '@(T,v)', '@(x)'); 
        f_str = strrep(f_str, 'T', 'x(1)'); 
        f_str = strrep(f_str, 'v', 'x(2)');
        f = str2func(f_str);

        % Use the Trust-Region-Dogleg algorithm to get v_2, T_2:
        if show_fsolve
            S = fsolve(f, [prefactor*T_2_IG_CP(index_EoS, num_run); prefactor*v_2_IG_CP(index_EoS, num_run)], optimoptions('fsolve', 'Display', 'iter', 'MaxFunctionEvaluations', iterations_fsolve_max));
        else
            S = fsolve(f, [prefactor*T_2_IG_CP(index_EoS, num_run); prefactor*v_2_IG_CP(index_EoS, num_run)], optimoptions('fsolve', 'MaxFunctionEvaluations', iterations_fsolve_max));
        end
        v_2(index_EoS, num_run) = S(2); T_2(index_EoS, num_run) = S(1);

        % Compute real-gas post-shock values and shock ratios
        v = v_2(index_EoS, num_run); 
        T = T_2(index_EoS, num_run);
        p_2(index_EoS, num_run) = double(subs(p));
        c_2(index_EoS, num_run) = double(subs(c));

        p2p1(index_EoS, num_run) = p_2(index_EoS, num_run) / p_1(index_EoS, num_run);
        v2v1(index_EoS, num_run) = v_2(index_EoS, num_run) / v_1(index_EoS, num_run);
        T2T1(index_EoS, num_run) = T_2(index_EoS, num_run) / T_1(index_EoS, num_run);
        u2u1(index_EoS, num_run) = v2v1(index_EoS, num_run); % true by definition from the continuity equation

        u_2(index_EoS, num_run) = u_1(index_EoS, num_run) * u2u1(index_EoS, num_run);
        Ma_2(index_EoS, num_run) = u_2(index_EoS, num_run) / c_2(index_EoS, num_run);
        Ma2Ma1(index_EoS, num_run) = Ma_2(index_EoS, num_run) / Ma_1(index_EoS, num_run);

        c_p_2_IG_TP(index_EoS, num_run) = double(subs(c_p_IG_TP));
        c_v_2_IG_TP(index_EoS, num_run) = double(subs(c_v_IG_TP));
        c_p_2(index_EoS, num_run) = double(subs(c_p));
        c_v_2(index_EoS, num_run) = double(subs(c_v));

        h_2_IG_TP(index_EoS, num_run) = double(subs(h_IG_TP));
        h_2(index_EoS, num_run) = double(subs(h));

        delta_h_2_IG_TP(index_EoS, num_run) = double(subs(delta_h_IG_TP));
        delta_h_2_miller(index_EoS, num_run) = double(subs(delta_h_miller));

        eqn_ns_1_soln_error(index_EoS, num_run) = p_2(index_EoS, num_run) - p_1(index_EoS, num_run) - (u_1(index_EoS, num_run)^2/(v_1(index_EoS, num_run)/M(index_EoS, num_run))^2)*((v_1(index_EoS, num_run)/M(index_EoS, num_run))-(v_2(index_EoS, num_run)/M(index_EoS, num_run)));
        eqn_ns_2_soln_error(index_EoS, num_run) = (h_2(index_EoS, num_run) - h_1(index_EoS, num_run))/M(index_EoS, num_run) - (u_1(index_EoS, num_run)^2/2)*(1-v_2(index_EoS, num_run)^2/v_1(index_EoS, num_run)^2);

        v_vec = v_2(index_EoS, num_run)*lower_lim_factor(index_EoS, num_run):v_2(index_EoS, num_run)*(upper_lim_factor(index_EoS, num_run)-lower_lim_factor(index_EoS, num_run))/(num_intervals_per_var(index_EoS, num_run)-1):v_2(index_EoS, num_run)*upper_lim_factor(index_EoS, num_run);
        T_vec = T_2(index_EoS, num_run)*lower_lim_factor(index_EoS, num_run):T_2(index_EoS, num_run)*(upper_lim_factor(index_EoS, num_run)-lower_lim_factor(index_EoS, num_run))/(num_intervals_per_var(index_EoS, num_run)-1):T_2(index_EoS, num_run)*upper_lim_factor(index_EoS, num_run);
        [V_VEC T_VEC] = meshgrid(v_vec, T_vec);

        % COMPUTE SENSITIVITY ANALYSIS OF THE SOLUTION
        % -------------------------------------------------------------------------
        fprintf("\tComputing sensitivity of real-gas post-shock conditions to [v,T] -- this may take a few moments...\n");
        fprintf("\t\tSensitivity analysis %2.0f %% complete...\n", 0); 
        clear c_p_IG_vec c_v_IG_vec c_p_vec c_v_vec c_vec h_IG_TP_vec delta_h_IG_TP_vec delta_h_miller_vec h_vec eqn_ns_1_vec eqn_ns_2_vec
        for i = 1:length(v_vec)
            for j = 1:length(T_vec)
                v = v_vec(i);
                T = T_vec(j);
                c_p_IG_vec(j,i) = double(subs(c_p_IG_TP));
                c_v_IG_vec(j,i) = double(subs(c_v_IG_TP));
                c_p_vec(j,i) = double(subs(c_p));
                c_v_vec(j,i) = double(subs(c_v));
                c_vec(j,i) = double(subs(c));
                h_IG_TP_vec(j,i) = double(subs(h_IG_TP));
                delta_h_IG_TP_vec(j,i) = double(subs(delta_h_IG_TP));
                delta_h_miller_vec(j,i) = double(subs(delta_h_miller));
                h_vec(j,i) = double(subs(h));
                eqn_ns_1_vec(j,i) = double(subs(eqn_ns_1));
                eqn_ns_2_vec(j,i) = double(subs(eqn_ns_2));
            end
            fprintf("\t\tSensitivity analysis %2.0f %% complete...\n", i*100/length(v_vec));
        end

        %% OUTPUTS - COMMAND WINDOW
        fprintf("\nNow outputting command window results...\n");

        fprintf("=============================================================\n");
        fprintf("PRE-SHOCK\n");
        fprintf("=============================================================\n");

        fprintf("\nThermodynamics:\n");
        fprintf("\tp_1_IG = %f\t\tp_1 = %f [Pa]\n", p_1_IG(index_EoS, num_run), p_1(index_EoS, num_run));
        fprintf("\tp_1_IG/p_c = %f\t\tp_1/p_c = %f\n", p_1_IG(index_EoS, num_run)/p_c(index_EoS, num_run), p_1(index_EoS, num_run)/p_c(index_EoS, num_run));
        fprintf("\tv_1 = %f [m^3 mol^-1] (rho_1 = %f [kg m^-3])\n", v_1(index_EoS, num_run), rho_1(index_EoS, num_run));
        fprintf("\tT_1 = %f [K]\n", T_1(index_EoS, num_run));
        fprintf("\tT_1/T_c = %f\n", T_1(index_EoS, num_run)/T_c(index_EoS, num_run));
        fprintf("\tu_1 = %f [m s^-1]\n", u_1(index_EoS, num_run));
        fprintf("\tc_1_IG_CP = %f\t\t\tc_1 = %f [m s^-1]\n", c_1_IG_CP(index_EoS, num_run), c_1(index_EoS, num_run));
        fprintf("\tMa_1_IG_CP = %f\t\t\tMa_1 = %f\n", Ma_1_IG_CP(index_EoS, num_run), Ma_1(index_EoS, num_run));

        fprintf("\n\tc_p_1_IG_TP = %f\t\tc_p_1 = %f [J kmol^-1 K^-1]\n", c_p_1_IG_TP(index_EoS, num_run), c_p_1(index_EoS, num_run)); 
        fprintf("\tc_v_1_IG_TP = %f\t\tc_v_1 = %f [J kmol^-1 K^-1]\n", c_v_1_IG_TP(index_EoS, num_run), c_v_1(index_EoS, num_run));
        fprintf("\th_1_IG_TP = %f\t\th_1 = %f [J kmol^-1]\n", h_1_IG_TP(index_EoS, num_run), h_1(index_EoS, num_run));
        fprintf("\tdelta_h_1_IG_TP = %f [J kmol^-1]\n", delta_h_1_IG_TP(index_EoS, num_run));
        fprintf("\tdelta_h_1_miller = %f [J kmol^-1]\n", delta_h_1_miller(index_EoS, num_run));

        fprintf("\nReal-gas derivatives and integrals:\n");
        fprintf("\texp_dpdT_1 = %f [Pa K^-1]\n", exp_dpdT_1(index_EoS, num_run));
        fprintf("\texp_dpdv_1 = %f [J kmol^-1]\n", exp_dpdv_1(index_EoS, num_run));
        fprintf("\texp_d2pdT2 = %f [Pa K^-2]\n", exp_d2pdT2_1(index_EoS, num_run));
        fprintf("\texp_intdpdT_1 = %f [J kmol^-1 K^-1]\n", exp_intdpdT_1(index_EoS, num_run));
        fprintf("\texp_intvdpdv_1 = %f [J kmol^-1]\n", exp_intvdpdv_1(index_EoS, num_run));
        fprintf("\texp_intd2pdT2 = %f [J kmol^-1 K^-2]\n", exp_intd2pdT2_1(index_EoS, num_run));

        fprintf("\n=============================================================\n");
        fprintf("POST-SHOCK\n");
        fprintf("=============================================================\n");

        fprintf("\nThermodynamics:\n");
        fprintf("\tp_2_IG_CP = %f\t\tp_2 = %f [Pa]\n", p_2_IG_CP(index_EoS, num_run), p_2(index_EoS, num_run));
        fprintf("\tp_2_IG_CP/p_c = %f\t\tp_2/p_c = %f\n", p_2_IG_CP(index_EoS, num_run)/p_c(index_EoS, num_run), p_2(index_EoS, num_run)/p_c(index_EoS, num_run));
        fprintf("\tv_2_IG_CP = %f\t\t\tv_2 = %f [m^3 mol^-1]\n", v_2_IG_CP(index_EoS, num_run), v_2(index_EoS, num_run));
        fprintf("\tT_2_IG_CP = %f\t\t\tT_2 = %f [K]\n", T_2_IG_CP(index_EoS, num_run), T_2(index_EoS, num_run));
        fprintf("\tT_2_IG_CP/T_c = %f\t\tT_2/T_c = %f\n", T_2_IG_CP(index_EoS, num_run)/T_c(index_EoS, num_run), T_2(index_EoS, num_run)/T_c(index_EoS, num_run));
        fprintf("\tu_2_IG_CP = %f\t\t\tu_2 = %f [m s^-1] \n", u_2_IG_CP(index_EoS, num_run), u_2(index_EoS, num_run));
        fprintf("\tc_2_IG_CP = %f\t\t\tc_2 = %f [m s^-1] \n", c_2_IG_CP(index_EoS, num_run), c_2(index_EoS, num_run));
        fprintf("\tMa_2_IG_CP = %f\t\t\tMa_2 = %f\n", Ma_2_IG_CP(index_EoS, num_run), Ma_2(index_EoS, num_run));

        fprintf("\n\tc_p_2_IG_TP = %f\t\tc_p_2 = %f [J kmol^-1 K^-1]\n", c_p_2_IG_TP(index_EoS, num_run), c_p_2(index_EoS, num_run)); 
        fprintf("\tc_v_2_IG_TP = %f\t\tc_v_2 = %f [J kmol^-1 K^-1]\n", c_v_2_IG_TP(index_EoS, num_run), c_v_2(index_EoS, num_run));
        fprintf("\th_2_IG_TP = %f\t\th_2 = %f [J kmol^-1]\n", h_2_IG_TP(index_EoS, num_run), h_2(index_EoS, num_run));
        fprintf("\tdelta_h_2_IG_TP = %f [J kmol^-1]\n", delta_h_2_IG_TP(index_EoS, num_run));
        fprintf("\tdelta_h_2_miller = %f [J kmol^-1]\n", delta_h_2_miller(index_EoS, num_run));

        fprintf("\n=============================================================\n");
        fprintf("RATIOS OF P,V,T,u\n");
        fprintf("=============================================================\n\n");

        fprintf("\tp2p1_IG_CP = %f\t\t\tp2p1 = %f \n", p2p1_IG_CP(index_EoS, num_run), p2p1(index_EoS, num_run));
        fprintf("\tv2v1_IG_CP = %f\t\t\tv2v1 = %f \n", v2v1_IG_CP(index_EoS, num_run), v2v1(index_EoS, num_run));
        fprintf("\tT2T1_IG_CP = %f\t\t\tT2T1 = %f \n", T2T1_IG_CP(index_EoS, num_run), T2T1(index_EoS, num_run));
        fprintf("\tu2u1_IG_CP = %f\t\t\tu2u1 = %f \n", u2u1_IG_CP(index_EoS, num_run), u2u1(index_EoS, num_run));


        %% OUTPUTS - SENSITIVITY ANALYSIS GRAPHS
        fprintf("\nNow plotting sensitivity analysis graphs...\n");

        if show_graph_sensitivity_cpcv
            figure; 
            s1 = surf(V_VEC, T_VEC, c_p_IG_vec, 'EdgeColor', 'black', 'FaceColor', [255,0,0]/255);
            hold on;
            s2 = surf(V_VEC, T_VEC, c_v_IG_vec, 'EdgeColor', 'black', 'FaceColor', [0,0,255]/255);
            s3 = surf(V_VEC, T_VEC, c_p_vec, 'EdgeColor', 'black', 'FaceColor', [255,150,0]/255);
            s4 = surf(V_VEC, T_VEC, c_v_vec, 'EdgeColor', 'black', 'FaceColor', [0,150,255]/255);
            xlabel('v [m^3 mol^{-1}]');
            ylabel('T [K]');
            zlabel('[kJ kmol^{-1} K^{-1}]');
            legend([s1, s2, s3, s4], {'c''_p','c''_v','c_p','c_v'});
            title('Specific Heat Capacities');
        end
        if show_graph_sensitivity_c
            figure;
            surf(V_VEC, T_VEC, real(c_vec));
            hold on;
            xlabel('v [m^3 mol^{-1}]');
            ylabel('T [K]');
            zlabel('[m s^{-1}]');
            title('Real-Gas Speed of Sound');
        end
        if show_graph_sensitivity_h_IG
            figure;
            surf(V_VEC, T_VEC, h_IG_TP_vec);
            hold on;
            xlabel('v [m^3 mol^{-1}]');
            ylabel('T [K]');
            zlabel('[kJ kmol^{-1}]');
            title('Ideal Gas Enthalpy');
        end
        if show_graph_sensitivity_deltah
            figure;
            s5 = surf(V_VEC, T_VEC, real(delta_h_IG_TP_vec), 'EdgeColor', 'black', 'FaceColor', 'red');
            hold on;
            s6 = surf(V_VEC, T_VEC, delta_h_miller_vec, 'EdgeColor', 'black', 'FaceColor', 'blue');
            xlabel('v [m^3 mol^{-1}]');
            ylabel('T [K]');
            zlabel('[kJ kmol^{-1}]');
            legend([s5, s6], {'\Deltah''','\Deltah''_{miller}'});
            title('Enthalpy Departure Functions');
        end
        if show_graph_sensitivity_h
            figure;
            surf(V_VEC, T_VEC, h_vec);
            hold on;
            xlabel('v [m^3 mol^{-1}]');
            ylabel('T [K]');
            zlabel('[kJ kmol^{-1}]');
            title('Real Gas Enthalpy');
        end
        if show_graph_sensitivity_goveqn && num_run == index_TRAC
            figure;

            subplot(1,2,1)
            s7 = surf(V_VEC, T_VEC, eqn_ns_1_vec, 'EdgeColor', 'black', 'FaceColor', 'red');
            hold on;
            s8 = scatter3(v_2(index_EoS, num_run),T_2(index_EoS, num_run),eqn_ns_1_soln_error(index_EoS, num_run), 150, [0, 255, 255]/255, 'filled');
            xlabel('\nu_2');
            ylabel('T_2');
            zlabel('Continuity-Momentum');
            set(gca, 'fontsize', size_font_sensitivity);
            % legend([s7, s8], {'Continuity-Momentum Equation', 'TRD Solution for CME'}, 'Location', 'SouthOutside');

            subplot(1,2,2)
            s9 = surf(V_VEC, T_VEC, real(eqn_ns_2_vec), 'EdgeColor', 'black', 'FaceColor', 'blue');
            hold on;
            s10 = scatter3(v_2(index_EoS, num_run),T_2(index_EoS, num_run),eqn_ns_2_soln_error(index_EoS, num_run), 150, [50, 200, 50]/255, 'filled');
            xlabel('\nu_2');
            ylabel('T_2');
            zlabel('Continuity-Energy');
            set(gcf, 'Position', [200, 200, 900, 400]);
            set(gca, 'fontsize', size_font_sensitivity);
            % legend([s9, s10], {'Continuity-Energy Equation', 'TRD Solution for CEE'}, 'Location', 'SouthOutside'); 
        end

        fprintf("\nSensitivity analysis graphs complete...\n");

        fprintf("\n-------------------------------------------------------------\n");
        fprintf("-------------------------------------------------------------\n\n");
    end
end


%% OUTPUTS - BATCH ANALYSIS GRAPHS
fprintf("\nNow displaying final graphs...\n");

if show_graphs_final
    figure;
    subplot(2,2,1); hold on;
    plot(Ma_1_IG_CP(1,:), p2p1_IG_CP(1,:), '-', 'Color', color_ideal);
    plot(Ma_1(1,:), p2p1(1,:), '-', 'Color', color_RK);
    plot(Ma_1(2,:), p2p1(2,:), '-', 'Color', color_SRK);
    plot(Ma_1(3,:), p2p1(3,:), '-', 'Color', color_PR);
    plot(Ma_1_IG_CP(1,index_SUBC), p2p1_IG_CP(1,index_SUBC), 'd', 'Color', color_ideal);
    plot(Ma_1_IG_CP(1,index_TRAC), p2p1_IG_CP(1,index_TRAC), 'o', 'Color', color_ideal);
    plot(Ma_1_IG_CP(1,index_SUPC), p2p1_IG_CP(1,index_SUPC), 's', 'Color', color_ideal);
    plot(Ma_1(1,index_SUBC), p2p1(1,index_SUBC), 'd', 'Color', color_RK);
    plot(Ma_1(1,index_TRAC), p2p1(1,index_TRAC), 'o', 'Color', color_RK);
    plot(Ma_1(1,index_SUPC), p2p1(1,index_SUPC), 's', 'Color', color_RK);
    plot(Ma_1(2,index_SUBC), p2p1(2,index_SUBC), 'd', 'Color', color_SRK);
    plot(Ma_1(2,index_TRAC), p2p1(2,index_TRAC), 'o', 'Color', color_SRK);
    plot(Ma_1(2,index_SUPC), p2p1(2,index_SUPC), 's', 'Color', color_SRK);
    plot(Ma_1(3,index_SUBC), p2p1(3,index_SUBC), 'd', 'Color', color_PR);
    plot(Ma_1(3,index_TRAC), p2p1(3,index_TRAC), 'o', 'Color', color_PR);
    plot(Ma_1(3,index_SUPC), p2p1(3,index_SUPC), 's', 'Color', color_PR);
%     xlim([xlim_low xlim_high]);
%     xticks(xlim_low:0.5:xlim_high);
%     ylim([0 15]);
%     yticks(0:5:15);
    ylabel('p_2/p_1');
    set(gca, 'fontsize', size_font);
    
    subplot(2,2,2); hold on;
    plot(Ma_1_IG_CP(1,:), v2v1_IG_CP(1,:), '-', 'Color', color_ideal);
    plot(Ma_1(1,:), v2v1(1,:), '-', 'Color', color_RK);
    plot(Ma_1(2,:), v2v1(2,:), '-', 'Color', color_SRK);
    plot(Ma_1(3,:), v2v1(3,:), '-', 'Color', color_PR);
    plot(Ma_1_IG_CP(1,index_SUBC), v2v1_IG_CP(1,index_SUBC), 'd', 'Color', color_ideal);
    plot(Ma_1_IG_CP(1,index_TRAC), v2v1_IG_CP(1,index_TRAC), 'o', 'Color', color_ideal);
    plot(Ma_1_IG_CP(1,index_SUPC), v2v1_IG_CP(1,index_SUPC), 's', 'Color', color_ideal);
    plot(Ma_1(1,index_SUBC), v2v1(1,index_SUBC), 'd', 'Color', color_RK);
    plot(Ma_1(1,index_TRAC), v2v1(1,index_TRAC), 'o', 'Color', color_RK);
    plot(Ma_1(1,index_SUPC), v2v1(1,index_SUPC), 's', 'Color', color_RK);
    plot(Ma_1(2,index_SUBC), v2v1(2,index_SUBC), 'd', 'Color', color_SRK);
    plot(Ma_1(2,index_TRAC), v2v1(2,index_TRAC), 'o', 'Color', color_SRK);
    plot(Ma_1(2,index_SUPC), v2v1(2,index_SUPC), 's', 'Color', color_SRK);
    plot(Ma_1(3,index_SUBC), v2v1(3,index_SUBC), 'd', 'Color', color_PR);
    plot(Ma_1(3,index_TRAC), v2v1(3,index_TRAC), 'o', 'Color', color_PR);
    plot(Ma_1(3,index_SUPC), v2v1(3,index_SUPC), 's', 'Color', color_PR);
%     xlim([xlim_low xlim_high]);
%     xticks(xlim_low:0.5:xlim_high);
%     ylim([0.2 0.5]);
%     yticks(0.2:0.1:0.5);
    ylabel('\nu_2/\nu_1');
    set(gca, 'fontsize', size_font);
    
    subplot(2,2,3); hold on;
    plot(Ma_1_IG_CP(1,:), T2T1_IG_CP(1,:), '-', 'Color', color_ideal);
    plot(Ma_1(1,:), T2T1(1,:), '-', 'Color', color_RK);
    plot(Ma_1(2,:), T2T1(2,:), '-', 'Color', color_SRK);
    plot(Ma_1(3,:), T2T1(3,:), '-', 'Color', color_PR);
    plot(Ma_1_IG_CP(1,index_SUBC), T2T1_IG_CP(1,index_SUBC), 'd', 'Color', color_ideal);
    plot(Ma_1_IG_CP(1,index_TRAC), T2T1_IG_CP(1,index_TRAC), 'o', 'Color', color_ideal);
    plot(Ma_1_IG_CP(1,index_SUPC), T2T1_IG_CP(1,index_SUPC), 's', 'Color', color_ideal);
    plot(Ma_1(1,index_SUBC), T2T1(1,index_SUBC), 'd', 'Color', color_RK);
    plot(Ma_1(1,index_TRAC), T2T1(1,index_TRAC), 'o', 'Color', color_RK);
    plot(Ma_1(1,index_SUPC), T2T1(1,index_SUPC), 's', 'Color', color_RK);
    plot(Ma_1(2,index_SUBC), T2T1(2,index_SUBC), 'd', 'Color', color_SRK);
    plot(Ma_1(2,index_TRAC), T2T1(2,index_TRAC), 'o', 'Color', color_SRK);
    plot(Ma_1(2,index_SUPC), T2T1(2,index_SUPC), 's', 'Color', color_SRK);
    plot(Ma_1(3,index_SUBC), T2T1(3,index_SUBC), 'd', 'Color', color_PR);
    plot(Ma_1(3,index_TRAC), T2T1(3,index_TRAC), 'o', 'Color', color_PR);
    plot(Ma_1(3,index_SUPC), T2T1(3,index_SUPC), 's', 'Color', color_PR);
    xlabel('Ma_1');
%     xlim([xlim_low xlim_high]);
%     xticks(xlim_low:0.5:xlim_high);
    ylabel('T_2/T_1');
%     ylim([1 2.25]);
%     yticks(1:0.25:2.25);
    set(gca, 'fontsize', size_font);
    
    subplot(2,2,4); hold on;
    plot(Ma_1_IG_CP(1,:), Ma2Ma1_IG_CP(1,:), '-', 'Color', color_ideal);
    plot(Ma_1(1,:), Ma2Ma1(1,:), '-', 'Color', color_RK);
    plot(Ma_1(2,:), Ma2Ma1(2,:), '-', 'Color', color_SRK);
    plot(Ma_1(3,:), Ma2Ma1(3,:), '-', 'Color', color_PR);
    plot(Ma_1_IG_CP(1,index_SUBC), Ma2Ma1_IG_CP(1,index_SUBC), 'd', 'Color', color_ideal);
    plot(Ma_1_IG_CP(1,index_TRAC), Ma2Ma1_IG_CP(1,index_TRAC), 'o', 'Color', color_ideal);
    plot(Ma_1_IG_CP(1,index_SUPC), Ma2Ma1_IG_CP(1,index_SUPC), 's', 'Color', color_ideal);
    plot(Ma_1(1,index_SUBC), Ma2Ma1(1,index_SUBC), 'd', 'Color', color_RK);
    plot(Ma_1(1,index_TRAC), Ma2Ma1(1,index_TRAC), 'o', 'Color', color_RK);
    plot(Ma_1(1,index_SUPC), Ma2Ma1(1,index_SUPC), 's', 'Color', color_RK);
    plot(Ma_1(2,index_SUBC), Ma2Ma1(2,index_SUBC), 'd', 'Color', color_SRK);
    plot(Ma_1(2,index_TRAC), Ma2Ma1(2,index_TRAC), 'o', 'Color', color_SRK);
    plot(Ma_1(2,index_SUPC), Ma2Ma1(2,index_SUPC), 's', 'Color', color_SRK);
    plot(Ma_1(3,index_SUBC), Ma2Ma1(3,index_SUBC), 'd', 'Color', color_PR);
    plot(Ma_1(3,index_TRAC), Ma2Ma1(3,index_TRAC), 'o', 'Color', color_PR);
    plot(Ma_1(3,index_SUPC), Ma2Ma1(3,index_SUPC), 's', 'Color', color_PR);
    xlabel('Ma_1');
%     xlim([xlim_low xlim_high]);
%     xticks(xlim_low:0.5:xlim_high);
    ylabel('Ma_2/Ma_1');
%     ylim([0.1 0.4]);
%     yticks(0.1:0.1:0.4);
    set(gca, 'fontsize', size_font);
    
    % Redundant figure just to extract legend
    figure; hold on;
    plot(Ma_1_IG_CP(1,:), Ma2Ma1_IG_CP(1,:), '-', 'Color', color_ideal);
    plot(Ma_1(1,:), Ma2Ma1(1,:), '-', 'Color', color_RK);
    plot(Ma_1(2,:), Ma2Ma1(2,:), '-', 'Color', color_SRK);
    plot(Ma_1(3,:), Ma2Ma1(3,:), '-', 'Color', color_PR);
    legend('Ideal','RK','SRK','PR','Location', 'southoutside', 'Orientation', 'horizontal');
    set(gca, 'fontsize', size_font);
    
    figure; hold on;
    title('Trust-Region-Dogleg Error');
    plot(Ma_1(1,:), eqn_ns_1_soln_error(1,:), 'o');
    plot(Ma_1(1,:), eqn_ns_2_soln_error(1,:), '*');
    plot(Ma_1(2,:), eqn_ns_1_soln_error(2,:), 'o');
    plot(Ma_1(2,:), eqn_ns_2_soln_error(2,:), '*');
    plot(Ma_1(3,:), eqn_ns_1_soln_error(3,:), 'o');
    plot(Ma_1(3,:), eqn_ns_2_soln_error(3,:), '*');
    xlabel('Ma_1');
    ylabel('Error');
    legend('Continuity-Momentum [kJ kg^{-1}] RK', 'Continuity-Energy [Pa] RK', ...
        'Continuity-Momentum [kJ kg^{-1}] SRK', 'Continuity-Energy [Pa] SRK',...
        'Continuity-Momentum [kJ kg^{-1}] PR', 'Continuity-Energy [Pa] PR','Location', 'eastoutside');
end

%% END
fprintf('Program execution completed.\n');